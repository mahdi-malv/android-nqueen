package com.golnoosh.nqueens.genetic;

import java.util.Comparator;

/**
 * A comparator class to compare two chromosomes
 */
public class ChromosomeComparator implements Comparator<Chromosome> {

	@Override
	public int compare(Chromosome x, Chromosome y) {
		if (x.fitness == y.fitness) {
			return 0;
		} else if (x.fitness < y.fitness) {
			return 1;
		} else {
			return -1;
		}
	}
}