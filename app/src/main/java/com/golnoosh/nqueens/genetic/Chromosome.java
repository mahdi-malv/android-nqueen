package com.golnoosh.nqueens.genetic;

public final class Chromosome {
	public int[] genes;
	public int fitness;
	public double cumAvgFitness;

	/**
	 * Use this function to get a copy of your chromosome
	 * @return a new instance of chromosome having the copied values
	 */
	public Chromosome clone() {
		Chromosome varCopy = new Chromosome();
		varCopy.genes = this.genes;
		varCopy.fitness = this.fitness;
		varCopy.cumAvgFitness = this.cumAvgFitness;
		return varCopy;
	}
}