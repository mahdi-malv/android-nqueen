package com.golnoosh.nqueens.genetic;

/**
 * Java itself if `PassByCopy`. This class will simulate the ability to pass by reference in the language.
 *
 * For example:
 *
 * int a = 2;
 * function {
 *     a = 3
 * }
 * a; // still 2
 *
 * But using reference:
 *
 * Ref<Int> aRef = new Ref(2);
 * function {
 *     aRef.values = 3;
 * }
 * aRef.value; // It's changed to 3
 *
 * This is pass by reference.
 */
public final class RefObject<T> {
	public T value;

	public RefObject(T refArg) {
		value = refArg;
	}
}